					
1	Crianças no trânsito devem?	Correr	Andar no meio fio	Distrair-se	Ter muita atenção
2	O que são vias de trânsito?	Espaço para os pedestres andarem	Locais para atravessar a rua	Pontes sobre rios	Ruas, estradas e becos para circulação de veículos
3	Quem são os pedestres no trânsito?	Pessoas de carro	Pessoas de bicicleta	Pessoas em ônibus	Pessoas que andam a pé
4	Quem são os agentes de trânsito?	Passageiros de ônibus	Passageiros em carros	Motoristas de veículos	Pessoas responsáveis por fiscalizar as regras de trânsito
5	As placas servem para:	Enfeitar as ruas	Iluminar o trânsito	Controlar o fluxo de pedestres	Informar normas e regras no trânsito
6	Uma criança recém nascida pode ser transportada no colo da mãe dentro do veículo?	Sim	Sim, se ela estiver amamentando	Não, deve ficar deitada no banco de trás	Não. Deve ficar na cadeirinha bebê conforto.
7	Qual o nome do veículo que faz o transporte de pessoas doentes ou acidentadas?	Carro de socorro	Carro de apoio	Transportador de doentes	Ambulância
8	Qual item sonoro é utilizado pelos bombeiros para abrir passagem no trânsito?	Buzina	Sino	Rádio	Sirene
9	Qual é a cor padrão dos caminhões e ambulâncias dos bombeiros?	Verde	Azul e branco	Amarelo	Vermelha
10	As garagens de carros são bons lugares para brincar? Para que servem?	Sim, servem de esconderijo	Sim, caso estejam bem iluminadas	Não, servem para guardar coisas diversas	Não. Para guardar os veículos.
11	Porque não devemos brincar em estacionamentos?	Porque não tem parquinho	Podemos brincar de bicicleta sempre	Porque pode chover	Porque os veículos manobram em marcha ré,
12	Podemos colocar parte do corpo fora da janela do carro?	Sim, mas apenas as mãos.	Sim, desde que preste atenção.	Não, pois atrapalha o motorista.	Não, pois é proibido e perigoso.
13	Crianças devem andar sempre:	No banco da frente	No colo de adultos	Onde ela desejar	Na cadeirinha de acordo com sua faixa etária
14	Algumas pessoas tem preferência dentro dos ônibus e ao embarcar ou desembarcar. Quem são?	Não existe preferências	Somente idosos	Crianças	idosos, gestantes, crianças de colo e portadores de deficiência.
15	Quem está autorizado a dirigir um veículo?	Qualquer pessoa que saiba dirigir	Somente adultos	Todas as pessoas	quem possuir a Carteira Nacional de Habilitação.
16	É permitido dirigir após ter consumido bebida alcoólica?	Sim, se for pouca bebida	Sim, se ao motorista beber água antes	É permitido	Não, não é permitido
17	Que cor são as placas que indicam regras obrigatórias?	Verdes	Amarelas e pretas	Brancas com listras	Brancas com bordas vermelhas.
18	Os bancos da frente foram feitos para os adultos. A afirmação está certa ou errada?	Errado	Foram feitos para qualquer pessoa sentar	Para adultos e animais de estimação	certa
19	A partir de que idade as crianças podem sentar no banco da frente?	A partir dos 7 anos	A partir dos 6 anos	A partir dos 13 anos	A partir de 10 anos.
20	Qual a parte mais protegida do carro?	A parte da frente	Todas são iguais	As laterais	A parte de trás
21	É certo ou errado jogar lixo pela janela do carro?	Certo	Certo, caso não tenha lixeiras perto	Errado. Pare o carro e jogue no chão	Errado
22	O que devemos ter dentro do carro para guardar papéis de balas, latas ou embalagens vazias?	Uma caixa de papelão	Nada, jogue pela janela	Não é necessário pois você pode jogar na rua	um lixeirinho ou saco plástico..
23	Quem fica sentado quieto no seu banco é considerado um bom ou ruim passageiro?	Passageiro ruim	Um pedestre	Um passageiro chato	bom passageiro.
24	Gritos ou movimentos bruscos podem assustar o:	Cachorro	Os pedestres nas ruas	Os ciclistas	o motorista
25	Choros e manhas podem assustar o	Amigo	A mamãe	Os ciclistas	o motorista
26	Se você ficar em pé, em cima do banco, poderá se machucar seriamente no caso de um acidente. A afirmação está certa   ou errada?	Errado	Errada pois dentro do carro você esta seguro	Incorreta	Certa
27	Quem dirige pode ficar prestando atenção em você?	Sim, pode	Pode prestar atenção em qualquer coisa	Não, pode apenas ficar no celular	não, deve prestar atenção nos carros e pedestres.
28	É certo ou errado ficar com os braços ou cabeça para fora da janela do carro?	Certo	Certo e divertido	Errado, somente a cabeça pode ficar para fora da janela	errado
29	O que acontece com quem não estiver usando o cinto de segurança durante um acidente?	Nada	Sentir um pequeno desconforto	Não perceber o acidente	pode ser lançado para fora do carro.
30	Somente o motorista precisa usar o cinto de segurança no veículo. A afirmação está certa ou errada?	Correta	Esta correta pois ele é o mais importante	Certa	errada. Todos devem usar o cinto de segurança.