	Perguntas para o 8 ano	Resposta errada 1	Resposta errada 2	Resposta errada 3	Resposta certa
1	Para não haver confusão no trânsito, todos devemos respeitar:	Conselhos	Os mais velhos	Os amigos	Regras ou leis de trânsito.
2	Para tirar a Carteira Nacional de Habilitação o candidato deve conhecer bem as:	Ruas da cidade	Placas de sinalização	Cores do semáforo	Leis ou regras de trânsito.
3	Para dirigir é necessário ter qual documento?	Carteira de Identidade	Documento com foto	Certidão de nascimento	Carteira Nacional de Habilitação.
4	Qual é a idade mínima para poder tirar a Carteira Nacional de Habilitação?	16 anos	20 anos	15 anos	18 anos
5	Como é o nome do conjunto de leis e regras que todos devem respeitar no trânsito?	Regras de Direção	Conjunto de regras a se fazer	Lista de deveres de trânsito	Código de Trânsito Brasileiro.
6	É permitido dirigir após ter consumido bebida alcoólica?	Sim, se for pouca bebida	Sim, se ao motorista beber água antes	É permitido	Não, não é permitido
7	Porque os motoristas recebem uma multa?	Para receber o endereço da delegacia.	Para levar um castigo	Para ter do que se lembrar	Porque não cumpriram uma ou mais regras de trânsito.
8	Qual é o objetivo da multa?	Identificar o motorista	Dar um castigo	Prender o carro	Evitar que o mesmo erro seja feito novamente.
9	Quem está autorizado a dirigir um veículo?	Qualquer pessoa que saiba dirigir	Somente adultos	Todas as pessoas	Quem possuir a Carteira Nacional de Habilitação.
10	Qual a função do agente de trânsito?	Parar o trânsito	Apitar	Cuidar das estradas	Orientar a todos as regras de trânsito.
11	Ao andar de bicicleta devemos usar sempre os:	Equipamentos de filmagem	Fones de ouvido	Óculos de sol	Equipamentos de segurança.
12	Qual é o nome do item que indica as regras a serem seguidas nas ruas?	Outdoors	Faixas	Cartilhas	Placas.
13	É certo ou errado fazer manobras radicais na frente dos carros?	Certo	Certo e divertido	Errado, manobras devem ser feitas no trânsito	Errado.
14	Qual item de segurança é obrigatório o uso pelo ciclista?	Luvas	Roupas especiais	Óculos de sol	O capacete.
15	Os pneus da bicicleta sempre devem estar:	Lisos	Vazios	Limpos	Cheios.
16	Capacete, cotoveleiras, joelheiras ou luvas são equipamentos de:	Passeio	Moda	Beleza	Segurança ou proteção.
17	Além do capacete, indique mais três itens importantes para segurança dos ciclistas:	Fones de ouvido, óculos de sol e meias	Sirene, óculos de sol e rádio portátil	Celular, Fones de ouvido e capa	Farol, refletor e campainha.
18	Na rua, a bicicleta é um brinquedo ou um veículo:	Brinquedo.	Brinquedo, e tem preferência.	Só bicicletas menores são brinquedos	Veículo.
19	Onde o ciclista deve andar?	No meio da rua.	Onde ele quiser.	Na calçada.	No lado direito da rua, próximo ao meio fio.
20	O ciclista também deve respeitar o semáforo e a faixa de:	Apoio.	Avisos.	Velocidade.	Pedestre.
21	Quem anda mais rápido, o pedestre ou o ciclista?	Pedestre.	Os dois andam na mesma velocidade.	Os dois tem a mesma velocidade	Ciclista.
22	Andando de bicicleta você continua sendo um pedestre?	Sim	Sim, um pedestre condutor	Não, um motorista	Não, um ciclista
23	Como se chamam os trechos onde somente são permitidos aos ciclistas percorrerem?	Vias.	Avenidas.	Mão dupla	Ciclovias.
24	Qual é o sentido que o ciclista deve trafegar numa rodovia?	Qualquer sentido.	Ciclistas não podem trafegar em rodovias.	No sentido contrário dos carros.	No mesmo sentido dos carros.
25	Qual é o lado correto para desembarcar de um veículo que faz o transporte escolar?	Qualquer lado	Não existe lado correto	Ambos os lados	No lado da calçada.
26	É certo ou errado atravessar uma rua em que o semáforo para pedestres esteja na cor vermelha mas não esteja vindo nenhum veículo?	Certo.	Certo se você for correndo.	Errado apenas se algum veículo estiver passando.	É errado.
27	Posso atravessar a rua na faixa de pedestres quando o agente de trânsito está presente e me autoriza a fazê-lo?	Não.	Pode somente se ele apitar para você.	Pode atravessar em qualquer lugar.	Sim, pode.
28	Para atravessar a rua devemos utilizar sempre a:	Opção mais fácil.	Via rápida.	Faixa de limite.	Faixa de pedestres.
29	Quando não há calçada, onde devemos andar?	No meio da rua.	Em qualquer lugar.	Equilibrando-se no meio fio	Nas laterais da rua.
30	Ao subir ou descer do ônibus devemos esperar que ele:	Abra a porta em movimento.	Buzine.	Mova-se devagar.	Pare completamente.
31	É certo ou errado atravessar na frente do ônibus?	Sim, posso atravessar em qualquer lado.	Errado, deve-se atravessar atrás do veículo.	Sim, é livre atravessar.	Errado, deve-se esperar o ônibus sair do local e atravessar na faixa.
32	Qual é o único momento que devemos descer da calçada?	Para ajuntar objetos na rua.	Para pararmos um veículo.	Para correr entre os carros.	Para atravessar a rua.
33	Quando há um semáforo para pedestres no cruzamento, devemos esperar qual luz acender para atravessar?	Vermelha.	Amarelo.	Qualquer uma delas.	Verde.
34	Em qual lugar da rua os carros diminuem a velocidade, facilitando assim a travessia?	No meio.	Nos cantos.	No início.	No início e no fim.
35	A CNH(Carteira Nacional de Habilitação) permite ao mototista dirigir em qualquer país do mundo?	Sim, em qualquer país.	Sim, mas com algumas restrições.	Somente em países das Américas.	Não. Deve-se tirar a Permissão Internacional para dirigir.
36	A Direção Defensiva deve ser aplicada quando:	o motorista se encontra em perigo.	de vez em quando.	o motorista achar que deve.	sempre, pois é uma atitude de prevenção e valorização da vida no trânsito.
37	Em caso de atropelamento, o motorista deve:	seguir em frente para evitar complicações.	Parar apenas se for um caso grave.	Seguir em frente e quando estiver em segurança chamar o socorro para a vítima.	Parar imediatamente para prestar socorro.
38	Somente o motorista precisa usar o cinto de segurança no veículo. A afirmação está certa ou errada?	Correta	Esta correta pois ele é o mais importante	Errada. As crianças menores de 10 anos também devem usar cinto de segurança.	Errada. Todos os ocupantes devem usar o cinto de segurança.
39	O que os motoristas devem fazer com os seus carros para que eles sempre estejam em dia para serem usados com segurança?	Lavar toda a semana.	Calibrar os pneus.	Não precisa fazer nada além de andar.	Manutenção preventiva.
40	Qual o objetivo da Lei Seca ao volante?	Incentivar ao motorista a consumir menos álcool antes de dirigir.	Penalizar o motorista alcoolizado.	Permitir o consumo de bebida alcoólica moderadamente.	Alertar a sociedade para os perigos do álcool associado à direção.