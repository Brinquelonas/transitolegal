					
1	O que é lei seca?	Lei de trânsito que proibe andar de carro nas chuvas fortes	Lei de trânsito que proibi beber enquanto dirige	Lei de trânsito sobre seguranças no trânsito em caso de chuva	Lei de trânsito que proibe dirigir sob efeito de álcool ou outras drogas.
2	Porque é proibido beber antes de dirigir?	Porque ele pode passar mal com a bebida	Porque bem bebe fica mais agressivo	Porque a bebida deixa o motorista com sono	Porque o alcool altera os sentidos das pessoas, deixando o trânsito perigoso.
3	São boas condições do veículo, EXCETO:	Manter os pneus em bom estado de uso.	Fazer revisões regulares nos automoveis.	Observar sempre as lanternas e faróis.	Utilizar rodas amassadas ou trincadas.
4	Somente o motorista precisa usar o cinto de segurança no veículo. A afirmação está certa ou errada?	Correta	Esta correta pois ele é o mais importante	Certa	errada. Todos devem usar o cinto de segurança.
5	O que significa a cor vermelha?	Siga	Atenção	Dance	Pare
6	O que significa a cor amarela?	Pare	Siga	Corra	Atenção
7	O que significa a cor verde?	Pare	Atenção	Preserve a natureza	Siga
8	São atitudes seguras do pedestre, exceto:	Atravessar na faixa	Utilizar passarelas	Atenção	Andar no meio fio
9	Podemos colocar parte do corpo fora da janela do carro?	Sim, mas apenas as mãos.	Sim, desde que preste atenção.	Não, pois atrapalha o motorista.	Não, pois é proibido e perigoso.
10	Crianças devem andar sempre:	No banco da frente	No colo de adultos	Onde ela desejar	Na cadeirinha de acordo com sua faixa etária
11	São exemplos de veículos maiores cuidando dos menores, EXCETO:	Reduzir a velocidade quando outro veículo for ultrapassar.	Nunca realizar ultrapassagem em locais proibidos.	Dar preferência sempre	Dirigir próximo aos veículos menores.
12	O que é legislação de trânsito?	Leis sobre como dirigir veículos	Leis sobre o transporte de cargas nos veículos	Leis que orientam os pedestres.	Conjunto de leis e regras que determinam como cada pessoa deve se comportar no trânsito.
13	A quem se destina o bebê conforto?	Apenas aos recém nascidos	Aos recém nascidos apenas nas vias de trânsito rápido.	As crianças de até 5 anos de idade	Aos bebês do nascimento até completarem 13 kilos.
14	Quem deve usar o acento de elevação?	Crianças de 1 a 4 anos	Crianças menores de 2 anos	Crianças acima de 5 anos	Crianças entre 4 e 7 anos
15	A partir de qual idade não é mais necessário o uso de cadeirinha?	4 anos	5 anos	6 anos	7 anos, desde que já tenha 1,45m e 36kg no mínimo.
16	O que é airbag?	Componente de conforto dentro do veículo	Sistema de direção	Sistema de rodas	Componente de segurança veícular.
17	Existe airbbag para motoqueiros?	Não	Não, mas existem roupas que protegem	Sim, ele protege as rodas.	Sim, ele pode estar presente na moto, no capacete e nas roupas do motociclista.
18	É uma atitude de convivência harmoniosa no trânsito, EXCETO:	Reduzir a velocidade quando outro veículo for ultrapassar.	Parar o carro para priorizar o pedestre	Ajudar quem precisa atravessar a rua	Estacionar o carro em fila dupla.
19	São exemplos de acessibilidade no trânsito, EXCETO:	Marcação nas calçadas	Semaforos com sinais sonoros	Adaptação de ônibus e veículos	Ausências de rampas.
20	São exemplos de violência no trânsito, EXCETO:	Dirigir sem atenção	Desrespeitar os pedestres	Consumir bebida alcoolica e dirigir	Seguir corretamente a sinalização do trânsito
21	É um exemplo de sinalização no trânsito, por, EXCETO:	Placas	Semaforos	Sinais sonoros	Buzina
22	É um beneficio da biclicleta, EXCETO:	Faz bem a saúde	Pode ser utilizada como meio de transporte	Não polui o meio ambiente	Só pode ser utilizada por adultos
23	Pessoas com carrinhos de bebê ou cadeira de rodas devem   andar na:	Rua	Estrada	Ciclovia	Calçada
24	Qual é o único momento que devemos descer da calçada?	Para ajuntar objetos na rua	Para pararmos um veículo	Para correr entre os carros	para atravessar a rua
25	Andando de bicicleta você continua sendo um pedestre?	Sim	Sim, um pedestre condutor	Não, um motorista	Não, um ciclista
26	Quem anda mais rápido, o pedestre ou o ciclista?	Pedestre	Os dois andam na mesma velocidade	Os dois tem a mesma velocidade	Ciclista
27	Na rua, a bicicleta é um brinquedo ou um veículo:	Brinquedo	Brinquedo, e tem preferência	Só bicicletas menores são brinquedos	Veículo
28	Onde o ciclista deve andar?	No meio da rua	Onde ele quiser	Na calçada	no lado direito da rua, próximo ao meio fio.
29	Qual item de segurança é obrigatório o uso pelo ciclista?	Luvas	Roupas especiais	Óculos de sol	o capacete
30	É um exemplo de poluiçao causada pelos veículos, EXCETO:	Poluição do ar	Poluição sonora	Poluição visual	Biodisel