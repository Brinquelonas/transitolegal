﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizPlayer : Openable {

    public Text NameText;
    public Image Image;
    public Color Color;
    public AudioClip Audio;
    public PlayerQRColor QRColor;

    public int RightAnswers;
    public int WrongAnswers;
    public string Log;
    public float TotalTime;

}
