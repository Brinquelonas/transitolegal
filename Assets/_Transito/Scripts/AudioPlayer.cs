﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

public class AudioPlayer : MonoBehaviour {

    public AudioMixerGroup MixerGroup;

    private List<AudioSource> _audioSources = new List<AudioSource>();
    private List<AudioSource> _voiceSources = new List<AudioSource>();

    private static AudioPlayer _instance;
    public static AudioPlayer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<AudioPlayer>();
            if (_instance == null)
                _instance = new GameObject("AudioPlayer").AddComponent<AudioPlayer>();

            return _instance;
        }
    }

    private AudioSource _source;
    public AudioSource Source
    {
        get
        {
            if (_source == null)
                _source = GetComponent<AudioSource>();
            if (_source == null)
            {
                _source = gameObject.AddComponent<AudioSource>();
                _source.spatialBlend = 0;
                if (MixerGroup != null)
                    _source.outputAudioMixerGroup = MixerGroup;
            }

            return _source;
        }
    }

    public void PlayClip(AudioClip clip)
    {
        PlayClip(clip, null);
    }

    public void PlayClip(AudioClip clip, System.Action callback)
    {
        PlayClip(clip, 1, callback);
    }

    public void PlayClip(AudioClip clip, float volume = 1, System.Action callback = null)
    {
        PlayClip(clip, false, volume, callback);
    }

    public void PlayClip(AudioClip clip, bool voice, float volume = 1, System.Action callback = null)
    {
        //Source.PlayOneShot(clip, volume);

        AudioSource source;

        if (voice)
        {
            if (!_voiceSources.Exists((a) => a.clip == clip))
            {
                source = new GameObject(clip.name).AddComponent<AudioSource>();
                source.transform.SetParent(transform);
                source.spatialBlend = 0;
                source.clip = clip;

                _voiceSources.Add(source);

            }
            else
            {
                source = _voiceSources.Find((a) => a.clip == clip);
            }
        }
        else
        {
            if (!_audioSources.Exists((a) => a.clip == clip))
            {
                source = new GameObject(clip.name).AddComponent<AudioSource>();
                source.transform.SetParent(transform);
                source.spatialBlend = 0;
                source.clip = clip;

                _audioSources.Add(source);
            }
            else
            {
                source = _audioSources.Find((a) => a.clip == clip);
            }
        }

        /*if (source == null)
        {
            if (callback != null)
                callback();
            return;
        }*/

        source.volume = volume;
        source.Play();

        if (callback != null)
            StartCoroutine(CallCallback(callback, clip.length));
    }

    private IEnumerator CallCallback(System.Action callback, float time)
    {
        float t = 0;
        while (t < time)
        {
            t += Time.deltaTime;
            yield return null;
        }

        callback();
    }

    public static void Play(AudioClip clip)
    {
        Play(clip, null);
    }

    public static void Play(AudioClip clip, System.Action callback)
    {
        Instance.PlayClip(clip, callback);
    }

    public static void Play(AudioClip clip, float volume, System.Action callback = null)
    {
        Instance.PlayClip(clip, volume, callback);
    }

    public static void Play(string audioTag)
    {
        Play(audioTag, null);
    }

    public static void Play(string audioTag, System.Action callback)
    {
        Play(audioTag, 1, callback);
    }

    public static void Play(string audioTag, float volume, System.Action callback = null)
    {
        Play(audioTag, false, volume, callback);
    }

    public static void Play(string audioTag, bool voice, float volume, System.Action callback = null)
    {
        AudioClip clip = Resources.Load<AudioClip>(audioTag);
        Instance.PlayClip(clip, voice, volume, callback);
    }

    public static void PlayVoiceGeneral(string audioTag)
    {
        PlayVoiceGeneral(audioTag, null);
    }

    public static void PlayVoiceGeneral(string audioTag, System.Action callback)
    {
        StopVoice();
        Play("Voice/geral/" + audioTag, true, 1, callback);
    }

    public static void StopVoice()
    {
        for (int i = 0; i < Instance._voiceSources.Count; i++)
        {
            Instance._voiceSources[i].Stop();
        }
    }
}
