﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BookToggle : MonoBehaviour {

    public Text NameText;
    public TextAsset TSV;
    public string Name;

    public string TrueName
    {
        get
        {
            return NameText.text;
        }
    }

    private Toggle _toggle;
    public Toggle Toggle
    {
        get
        {
            if (_toggle == null)
                _toggle = GetComponent<Toggle>();

            return _toggle;
        }
    }

    private void Awake()
    {
        Toggle.onValueChanged.AddListener((isOn) => 
        {
            if (isOn)
                AudioPlayer.PlayVoiceGeneral(Name);
        });
    }

}
