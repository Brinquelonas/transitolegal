﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PotaTween))]
[CanEditMultipleObjects]
public class PotaTweenEditor : Editor
{

    public override void OnInspectorGUI()
    {
        PotaTween tween = target as PotaTween;

        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Tag: " + tween.Tag);
        EditorGUILayout.LabelField("ID: " + tween.Id);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        string props = "";
        if (tween.Position.From != tween.Position.To)
            props += ("Position\t");
        if (tween.Rotation.From != tween.Rotation.To)
            props += ("Rotation\t");
        if (tween.Scale.From != tween.Scale.To)
            props += ("Scale\t");
        if (tween.Color.From != tween.Color.To)
            props += ("Color\t");
        if (tween.Alpha.From != tween.Alpha.To)
            props += ("Alpha\t");
        EditorGUILayout.LabelField(props);
        GUILayout.EndHorizontal();


        GUILayout.BeginVertical("box");
        tween.foldout = EditorGUILayout.Foldout(tween.foldout, "Tween Configs");
        if (tween.foldout)
            base.OnInspectorGUI();
        GUILayout.EndVertical();
    }

}
