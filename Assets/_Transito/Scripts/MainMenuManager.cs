﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum GameMode
{
    AR = 0,
    QUIZ = 1
}

public enum PlayerQRColor
{
    Black,
    White
}

public static class GameConfig
{
    public static string BookName;
    public static string BookTrueName;
    public static TextAsset TSV;
    public static List<PlayerConfig> Players;
    public static GameMode GameMode;
}

public class PlayerConfig
{
    public string Name;
    public Sprite Sprite;
    public Color Color;
    public AudioClip Audio;
    public PlayerQRColor QRColor;

    public PlayerConfig(string name, Sprite sprite, Color color, AudioClip audio, PlayerQRColor qrColor)
    {
        Name = name;
        Sprite = sprite;
        Color = color;
        Audio = audio;
        QRColor = qrColor;
    }
}

public class MainMenuManager : MonoBehaviour {

    public Button OKButton;
    public Button BackButton;
    public Button PhotoButton;

    public GameModeSelection GameModeSelection;
    public BookSelection BookSelection;
    public PlayerSelection PlayerSelection;

    public PotaTween Fader;

    private static MainMenuManager _instance;
    public static MainMenuManager Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<MainMenuManager>();
            return _instance;
        }
    }

    private void Awake()
    {
        OKButton.onClick.AddListener(() => 
        {
            if (BookSelection.IsOpen)
            {
                BookSelection.OKButtonClicked();
            }
            else if (PlayerSelection.IsOpen)
            {
                PlayersSelected();
            }
        });

        BackButton.onClick.AddListener(() => 
        {
            if (GameModeSelection.IsOpen)
            {
                GameModeSelection.SetActive(false);
                PlayerSelection.SetActive(false);
                BookSelection.SetActive(false);

                Fader.gameObject.SetActive(true);
                Fader.Reverse(() =>
                {
                    Application.Quit();
                });
            }
            if (BookSelection.IsOpen)
            {
                GameModeSelection.SetActive(true);
                PlayerSelection.SetActive(false);
                BookSelection.SetActive(false);
                OKButton.gameObject.SetActive(false);
            }
            else if (PlayerSelection.IsOpen)
            {
                /*if (GameConfig.GameMode == GameMode.AR)
                {*/
                    GameModeSelection.SetActive(false);
                    PlayerSelection.SetActive(false);
                    BookSelection.SetActive(true);
                /*}
                else
                {
                    GameModeSelection.SetActive(true);
                    PlayerSelection.SetActive(false);
                    BookSelection.SetActive(false);
                    OKButton.gameObject.SetActive(false);
                }*/
            }
        });

        PhotoButton.onClick.AddListener(() => 
        {
            GameModeSelection.SetActive(false);
            BookSelection.SetActive(false);
            PlayerSelection.SetActive(false);

            Fader.gameObject.SetActive(true);
            Fader.Reverse(() => 
            {
                SceneManager.LoadScene("Photo");
            });
        });
    }

    private void Start()
    {
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;

        OKButton.gameObject.SetActive(false);
        Fader.Play(() =>
        {
            //BookSelection.SetActive(true);
            GameModeSelection.SetActive(true);
            Fader.gameObject.SetActive(false);
        });
    }

    public void GameModeSelected(int mode)
    {
        GameModeSelected((GameMode)mode);
    }

    public void GameModeSelected(GameMode mode)
    {
        GameConfig.GameMode = mode;

        GameModeSelection.SetActive(false);

        /*if (mode == GameMode.AR)
        {*/
            BookSelection.SetActive(true);
            PlayerSelection.SetActive(false);
        /*}
        else
        {
            BookSelection.SetActive(false);
            PlayerSelection.SetActive(true);

            GameConfig.BookName = "Quiz";
            GameConfig.BookTrueName = "Quiz";
            GameConfig.TSV = GameModeSelection.QuizTSV;
        }*/

        OKButton.gameObject.SetActive(true);
    }

    public void BookSelected()
    {
        GameModeSelection.SetActive(false);
        BookSelection.SetActive(false);
        PlayerSelection.SetActive(true);
    }

    public void PlayersSelected()
    {
        List<PlayerConfig> players = new List<PlayerConfig>();

        for (int i = 0; i < PlayerSelection.PlayerToggles.Count; i++)
        {
            if (PlayerSelection.PlayerToggles[i].Toggle.isOn)
            {
                if (string.IsNullOrEmpty(PlayerSelection.PlayerToggles[i].Name))
                    return;

                players.Add(new PlayerConfig(PlayerSelection.PlayerToggles[i].Name, PlayerSelection.PlayerToggles[i].Sprite, PlayerSelection.PlayerToggles[i].Color, PlayerSelection.PlayerToggles[i].Audio, PlayerSelection.PlayerToggles[i].QRColor));
            }
        }

        if (players.Count <= 0)
            return;

        GameConfig.Players = players;
        PlayerSelection.SetActive(false);

        Fader.gameObject.SetActive(true);
        Fader.Reverse(() =>
        {
            //SceneManager.LoadScene("Game");
            if (GameConfig.GameMode == GameMode.AR)
                SceneManager.LoadScene("Game");
            else
                SceneManager.LoadScene("GameQuiz");
        });
    }
}
