﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;
using UnityEngine;
using UnityEngine.Events;

public class MailSender {

    [System.Serializable]
    public class MailEvent : UnityEvent { }

    public MailEvent OnSuccess = new MailEvent();
    public MailEvent OnFail = new MailEvent();

    private System.Action _callback;

    public IEnumerator SendMailCoroutine(string toMail, string body)
    {
        yield return null;

        MailMessage mail = new MailMessage();

        //mail.From = new MailAddress("transitolegaldc@gmail.com");
        mail.From = new MailAddress("transitolegal@editoradc.com.br");
        //mail.From = new MailAddress("transitolegaldc@brinquelonas.com.br");
        mail.To.Add(toMail);
        mail.Subject = Application.productName + " " + System.DateTime.Now;

        mail.Body = "Relatório do jogo " + Application.productName + " realizado na data " + System.DateTime.Now + "\n\n" + body;

        SmtpClient smtpServer = new SmtpClient("smtp.editoradc.com.br");
        //SmtpClient smtpServer = new SmtpClient("smtp.brinquelonas.com.br");
        //SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        //smtpServer.Port = 25;
        //smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("transitolegaldc@gmail.com", "123456transitolegal");
        smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("transitolegal=editoradc.com.br", "246@editoradc");
        //smtpServer.Credentials = (System.Net.ICredentialsByHost)new System.Net.NetworkCredential("transitolegaldc@brinquelonas.com.br", "projetosabermais1");
        //smtpServer.EnableSsl = true;
        smtpServer.EnableSsl = false;
        System.Net.ServicePointManager.ServerCertificateValidationCallback =
            delegate (object s, System.Security.Cryptography.X509Certificates.X509Certificate certificate, System.Security.Cryptography.X509Certificates.X509Chain chai, System.Net.Security.SslPolicyErrors sslPolicyErrors)
            {
                return true;
            };

        bool success = true;
        try
        {
            smtpServer.Send(mail);
        }
        catch (System.Exception e)
        {
            success = false;
            Debug.Log(e.GetBaseException());
            //return false;
            OnFail.Invoke();
        }

        if (success)
        {
            OnSuccess.Invoke();
            yield break;
        }

        //return true;
        yield return null;
    }

    private void SendComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
    {
        string token = (string)e.UserState;
        
        if (e.Cancelled)
            OnFail.Invoke();
        if (e.Error != null)
            OnFail.Invoke();
        else
            OnSuccess.Invoke();
    }

}
