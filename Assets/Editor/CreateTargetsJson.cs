﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CreateTargetsJson : MonoBehaviour {    

    private static List<string> ImageNames = new List<string>();

    [MenuItem("EasyAR/Create Targets Json")]
	public static void Create()
    {
        ImageNames = new List<string>();

        foreach (string item in Directory.GetFiles(Application.streamingAssetsPath))
        {
            string itemName = item.Split('\\')[1];
            if (!itemName.EndsWith(".meta") && !itemName.EndsWith(".json"))
            {
                //Debug.Log(itemName);
                ImageNames.Add(itemName);
            }
        }

        string str = "{\n\t\t\"images:\"\n\t\t[\n\t\t\t";
        for (int i = 0; i < ImageNames.Count; i++)
        {
            str += "\"" + ImageNames[i] + "\"";
            if (i < ImageNames.Count - 1)
                str += ",\n\t\t\t";
            else
                str += "\n\t\t";
        }
        str += "]\n}";

        //print(str);

        string path = Application.streamingAssetsPath + "\\targets.json";
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }
        AssetDatabase.Refresh();
    }

}
